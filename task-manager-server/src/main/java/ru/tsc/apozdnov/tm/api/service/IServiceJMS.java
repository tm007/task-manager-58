package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.LogDto;

public interface IServiceJMS {

    void send(@NotNull LogDto entity);

    LogDto createMessage(@NotNull Object object, @NotNull String type);

}