package ru.tsc.apozdnov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.dto.request.DataXmlSaveFasterXmlRequest;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveFasterXmlListener.getName() == #event.name")
    public void executeEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}