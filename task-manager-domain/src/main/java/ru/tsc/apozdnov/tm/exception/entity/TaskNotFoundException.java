package ru.tsc.apozdnov.tm.exception.entity;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}